from django.db import models
import os
from django.contrib.auth.models import User
from django.db.models.signals import post_save, pre_delete, post_delete
from django.dispatch import receiver
from django.template.defaultfilters import slugify

# for generating filename of the uploaded question image
def question_image_filename(instance, filename):
    s = filename.split('.')[-1]# the extension
    return os.path.join(instance.chapter.name+'-'.instance.pk+'.'+s)


class ExamType(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Subject(models.Model):
    name            = models.CharField(max_length=50)
    exam            = models.ForeignKey('ExamType')
    marks_carried   = models.IntegerField(default=50)
    slug            = models.SlugField(blank=True)

    def save(self, *args, **kwargs):
        if not self.id:     # newly created
            self.slug=slugify(self.name)
        super(Subject, self).save(*args, **kwargs)

    def __str__(self):
        return self.name+' ('+self.exam.name+')'


class Chapter(models.Model):
    name            = models.CharField(max_length=50)
    subject         = models.ForeignKey('Subject')
    marks_carried   = models.IntegerField(default=50)
    slug            = models.SlugField(blank=True)

    def save(self, *args, **kwargs):
        if not self.id:     # newly created
            self.slug=slugify(self.name)
        super(Chapter, self).save(*args, **kwargs)

    def __str__(self):
        return self.name+' ('+self.subject.name+') ['+self.subject.exam.name+']'


class Question(models.Model):

    question_set    = models.ForeignKey('QuestionSet', # which question set it belongs to, if any
                                        null=True, blank=True)
    examtype        = models.ForeignKey('ExamType')# IOE or Indian Embassy or any
    text            = models.CharField(max_length=800)
    subject         = models.ForeignKey('Subject', blank=True, null=True)
    chapter         = models.ForeignKey('Chapter', blank=True, null=True)
    marks_carried   = models.IntegerField(default=1, null=True, blank=True)
    correct_answer  = models.IntegerField(default=1)
    asked_in_exam   = models.CharField(max_length=30, blank=True)
    question_image  = models.CharField(max_length=100, blank=True, null=True)

    option1         = models.CharField(max_length=500, blank=True, null=True)
    option2         = models.CharField(max_length=500, blank=True, null=True)
    option3         = models.CharField(max_length=500, blank=True, null=True)
    option4         = models.CharField(max_length=500, blank=True, null=True)

    #not images but locations are stored as images are parsed from doc
    option1_image   = models.CharField(max_length=100, null=True, blank=True)
    option2_image   = models.CharField(max_length=100, null=True, blank=True)
    option3_image   = models.CharField(max_length=100, null=True, blank=True)
    option4_image   = models.CharField(max_length=100, null=True, blank=True)

    hints           = models.TextField(blank=True)

    # for deleting images while deleting object if prsent
    def delete(self, *args, **kwargs):
        
        super(Question, self).delete(*args, **kwargs) 
        
        if self.question_image:
            storage, path = self.question_image.storage, \
                        self.question_image.path
            storage.delete(path)

        if self.option1_image:
            storage, path = self.option1_image.storage, \
                        self.option1_image.path
            storage.delete(path)
        if self.option2_image:
            storage, path = self.option2_image.storage, \
                        self.option2_image.path
            storage.delete(path)
        if self.option3_image:
            storage, path = self.option3_image.storage, \
                        self.option3_image.path
            storage.delete(path)
        if self.option4_image:
            storage, path = self.option4_image.storage, \
                        self.option4_image.path
            storage.delete(path)


class QuestionSet(models.Model):
    set_id = models.CharField(max_length=20, default='')
    
    '''
    def save(self):
        super(QuestionSet, self).save()
        self.set_id = 'set '+str(self.pk)
        super(QuestionSet, self).save()
    '''

    def __str__(self):
        return self.set_id


class Student(models.Model):
    user                = models.OneToOneField(User, primary_key=True, related_name="Student")
    verification_key    = models.CharField(max_length=10)
    account_verified    = models.BooleanField(default=False)
    progress            = models.FloatField(default=0.0)
    college             = models.CharField(max_length=100)
    correct_submissions = models.IntegerField(default=0)
    questions_done      = models.IntegerField(default=0)
    sets_completed      = models.ManyToManyField('QuestionSet')

    def __str__(self):
        return self.user.username

    def update_progress(self):
        # assumption that this is updated after every set
        # is completed and/or chapters are completed
        pass

# helper for DocFile class
def docfile_upload(instance, filename):
    s = filename.split('.')[-1] # to get the extension
    return os.path.join('docs/'+str(instance.pk)+'.'+s)

class DocFile(models.Model):
    doc = models.FileField(upload_to=docfile_upload, null=True)

    def delete(self, *args, **kwargs):
        storage, path = self.doc.storage, self.doc.path
        super(DocFile, self).delete(*args, **kwargs)
        os.remove("media/"+self.doc.name)
        storage.delete(path)

