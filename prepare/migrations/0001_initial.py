# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import prepare.models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Chapter',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=50)),
                ('marks_carried', models.IntegerField(default=50)),
                ('slug', models.SlugField(blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DocFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('doc', models.FileField(upload_to=prepare.models.docfile_upload, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ExamType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('text', models.CharField(max_length=800)),
                ('marks_carried', models.IntegerField(default=1, null=True, blank=True)),
                ('correct_answer', models.IntegerField(default=1)),
                ('asked_in_exam', models.CharField(max_length=30, blank=True)),
                ('question_image', models.CharField(max_length=100, null=True, blank=True)),
                ('option1', models.CharField(max_length=500, null=True, blank=True)),
                ('option2', models.CharField(max_length=500, null=True, blank=True)),
                ('option3', models.CharField(max_length=500, null=True, blank=True)),
                ('option4', models.CharField(max_length=500, null=True, blank=True)),
                ('option1_image', models.CharField(max_length=100, null=True, blank=True)),
                ('option2_image', models.CharField(max_length=100, null=True, blank=True)),
                ('option3_image', models.CharField(max_length=100, null=True, blank=True)),
                ('option4_image', models.CharField(max_length=100, null=True, blank=True)),
                ('hints', models.TextField(blank=True)),
                ('chapter', models.ForeignKey(null=True, blank=True, to='prepare.Chapter')),
                ('examtype', models.ForeignKey(to='prepare.ExamType')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='QuestionSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('set_id', models.CharField(max_length=20, default='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('user', models.OneToOneField(serialize=False, related_name='Student', primary_key=True, to=settings.AUTH_USER_MODEL)),
                ('verification_key', models.CharField(max_length=10)),
                ('account_verified', models.BooleanField(default=False)),
                ('progress', models.FloatField(default=0.0)),
                ('college', models.CharField(max_length=100)),
                ('correct_submissions', models.IntegerField(default=0)),
                ('questions_done', models.IntegerField(default=0)),
                ('sets_completed', models.ManyToManyField(to='prepare.QuestionSet')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Subject',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=50)),
                ('marks_carried', models.IntegerField(default=50)),
                ('slug', models.SlugField(blank=True)),
                ('exam', models.ForeignKey(to='prepare.ExamType')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='question',
            name='question_set',
            field=models.ForeignKey(null=True, blank=True, to='prepare.QuestionSet'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='question',
            name='subject',
            field=models.ForeignKey(null=True, blank=True, to='prepare.Subject'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='chapter',
            name='subject',
            field=models.ForeignKey(to='prepare.Subject'),
            preserve_default=True,
        ),
    ]
