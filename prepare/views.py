from django.shortcuts import render, redirect, Http404
from django.http import HttpResponse
from django.views.generic import View
from prepare.models import*
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import send_mail, EmailMultiAlternatives
from django.db.models import Q
from django.template import Context
import string
import random
from datetime import datetime
from .forms import *
import json

import subprocess
from .docxread import *

# the index view
class Home(View):
    context = {}
    def get(self, request):
        if request.user.is_authenticated() and not request.user.is_staff:
            return redirect('practices')

        return render(request, 'prepare/index.html', self.context)


def custom_login(request):
    context={}
    if not request.method=='POST':
        raise Http404('Wrong request')
    else:
        username=request.POST.get('username')
        password=request.POST.get('password')

        user = authenticate(username=username, password=password)

        if user is None:
            context['message'] = 'username/password error'
            return HttpResponse('bad login')
        else:
            student = Student.objects.get(user=user)

            if not student.account_verified:

                context['message'] = 'this account has not been verified, please verify by checking your email'
                return render(request, 'prepare/message.html', context)

            login(request, user)
            return redirect('practices')

class Practices(View):
    context={}
    def get(self, request):
        if request.user.is_authenticated() and not request.user.is_staff:
            user = request.user
            self.context['username'] = user.username
            try:
                subjects = Subject.objects.filter(exam__name='IOE')
                self.context['subjects'] = subjects
            except Exception:
                raise Http404('content not found')
            return render(request, "prepare/sets_subjects.html", self.context)
        else:
            return redirect('index')

def custom_logout(request):
    logout(request)
    return redirect('index')


class Register(View):
    context = {}
    form = RegistrationForm()
    def get(self, request):
        if not request.user.is_authenticated() or request.user.is_staff:
            self.context['form'] = self.form
            return render(request, 'prepare/register.html', self.context)
        else:
            return redirect('practices')


    def post(self, request):
        form = RegistrationForm(request.POST)

        if form.is_valid():
            # create a student object and send the validation key in the email
            # get the required fields from the form
            first_name=form.cleaned_data['first_name']
            last_name=form.cleaned_data['last_name']
            username=form.cleaned_data['username']
            password=form.cleaned_data['password']
            email=form.cleaned_data['email']
            college = form.cleaned_data['college']

            # check if there are existing users
            existing_users = User.objects.filter(
                                            Q(email=email) | \
                                            Q(username=username)
            )

            if len(existing_users)!=0:
                raise Http404('User already exists')

            try:
                user = User(username=username,
                            email=email, first_name=first_name, 
                            last_name=last_name, 
                            last_login=datetime.now()
                )
                user.set_password(password)
                user.save()

                # generate a new key for the user
                generated_key=verification_key_generate()

                Student.objects.create(user=user, college=college,
                                        verification_key=generated_key
                )

                # email subject, from and to
                subject, from_email, to = 'account verification', \
                                            'a@b.com', email
                # non html content of the email
                text_content = '''
                        Please click the link below to verify your account'
                '''
                # htmo content of the email
                html_content = text_content + \
                    '<p><a href=\"http://localhost:8000/prepare/verify/?key='+\
                    generated_key+\
                    '\" onclick=\"alert()\"> <strong>verification</strong></a></p>'

                msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
                # attach html element
                msg.attach_alternative(html_content, "text/html")

                msg.send()

                self.context['message'] = "Thank you!! You are registered.<br> Please check your mail for verification."
                return render(request, "prepare/message.html", self.context)
            except Exception:
                user.delete()
                return HttpResponse('something went wrong')
        else:
            self.context['form'] = form
            return render(request, 'prepare/register.html', self.context)


# for account verification
def verify_account(request):
    context = {}
    if request.method=='POST':
        return HttpResponse('not POST please!!')

    key = request.GET.get('key', '')
    student = Student.objects.filter(verification_key=key, account_verified=False)

    if len(student)!=1:
        return HttpResponse('sorry, verification can\'t be done.')
    else:
        student[0].account_verified=True
        student[0].save()
        context['message'] = "Your account has been verified"
        # login user and redirect to practices page
        return HttpResponse('your account has been verified')

# generates the validation key
def verification_key_generate(size=10, chars=string.ascii_uppercase+string.ascii_lowercase+string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


# this creates the questions objects from the docfile recently uploaded
def questions(docfile, subject, chapter, setname):
    filename = docfile.doc.name

    document = get_word_xml(filename) 
    rels = get_rels_xml(filename) # if images used

    doc_tree = get_xml_tree(document)
    rels_tree = get_xml_tree(rels)

    # delete the docfile, no longer needed
    docfile.delete()

    # get questions and answers
    questions_options_answers = get_questions_and_options(doc_tree, rels_tree)
    questions_options = questions_options_answers[0]
    answers = questions_options_answers[1]
    answers_list = parse_answers(answers)

    if setname=='':
        question_set = None
        try:
            subject = Subject.objects.filter(name__iexact=subject.lower(),
                        exam__name='IOE'
                    )[0]
            chapter = Chapter.objects.filter(name__iexact=chapter.lower(),
                        subject=subject
                    )[0]
        except Exception:
            raise Http404('no such chapter/subject')

    elif subject=='': # means set is being chosen
        # check if setname exists or not
        q_sets = QuestionSet.objects.filter(set_id=setname)
        if len(q_sets) != 0:
            return HttpResponse('the set name is already chosen<br> Please select another name')
        subject = None
        chapter = None
        question_set = QuestionSet(set_id=setname)
        question_set.save()
    else:
        raise Http404('something wrong')

    try:

        for x in range(len(questions_options)):
            question = questions_options[x]
            q_options = parse_question_and_options(question)

            question = Question(question_set=question_set, 
                                subject=subject, chapter=chapter)

            question.examtype = ExamType.objects.filter(name='IOE')[0]
            question.save() # we need its id
            question.correct_answer = ord(answers_list[x]) - ord('a') + 1

            text_and_image = get_text_and_image(q_options[0], question, 'qimage')
            question.text = text_and_image[0]
            question.question_image = text_and_image[1]

            text_and_image = get_text_and_image(q_options[1], question, 'opt1image')
            question.option1 = text_and_image[0]
            question.option1_image = text_and_image[1]

            text_and_image = get_text_and_image(q_options[2], question, 'opt2image')
            question.option2 = text_and_image[0]
            question.option2_image = text_and_image[1]

            text_and_image = get_text_and_image(q_options[3], question, 'opt3image')
            question.option3 = text_and_image[0]
            question.option3_image = text_and_image[1]
            
            text_and_image = get_text_and_image(q_options[4], question, 'opt4image')
            question.option4 = text_and_image[0]
            question.option4_image = text_and_image[1]

            question.save()

    except Exception:# clean things that are not complete
        if question_set:
            question_set.delete()
        if question:
            question.delete()
        subprocess.call(["rm", "extracted/docx/", '-r'])
        return HttpResponse('seems like parsing failed, please confirm the specifications of the document file')

    subprocess.call(["rm", "extracted/docx/", '-r'])
    return HttpResponse('questions saved')


class Admin(View):
    def get (self, request):
        if not request.user.is_authenticated() or not request.user.is_staff:
            return HttpResponse('please login as staff from admin')
        return render(request, "prepare/admin.html", {})

class DeleteSets(View):
    def get(self, request):
        if not request.user.is_authenticated() or not request.user.is_staff:
            return HttpResponse('please login as staff from admin')
        # get the sets present
        sets = QuestionSet.objects.all()
        return render(request, "prepare/delete.html", {'sets':sets})


class UploadQuestions(View):
    form = QuestionUploadForm()
    context = {}

    def get(self, request):
        if not request.user.is_authenticated() or not request.user.is_staff:
            return HttpResponse('please login as staff from admin')

        self.context['form'] = self.form
        return render(request, "prepare/upload.html", self.context)

    def post(self, request):
        form = QuestionUploadForm(request.POST, request.FILES)
        if form.is_valid():
            subject = request.POST.get('subject', '')
            chapter = request.POST.get('chapter', '')
            setname= request.POST.get('set_name', '')
            # check validity, i.e both setname and subject/chapter cant have values
            if setname!='':
                if subject!='' or chapter!='':
                    return HttpResponse('please select subject or set, not both')
            elif subject!='':
                if chapter=='':
                    return HttpResponse('select a chapter too')
            else:
                return HttpResponse('invalid set name')
            docfile = DocFile()
            docfile.save() # first save coz we need id
            docfile.doc = request.FILES['docfile']
            docfile.save()
            return questions(docfile, subject, chapter, setname)
        else:
            return HttpResponse('invalid form')


class Sets(View):
    context = {}
    def get(self, request):

        if not check_authentication(request):
            return redirect('index')

        # the student is authencicated
        set_id = request.GET.get('set_id', '')
        if set_id != '':
            try:
                question_set = QuestionSet.objects.get(set_id=set_id.strip())
                # get questions and pass to the template
                # ... to be done ...
            except Exception:# display the general page
                pass
        # the general page
        student = Student.objects.get(user=request.user)
        practiced_sets = set(student.sets_completed.all())
        all_sets = set(QuestionSet.objects.all())
        unpracticed_sets = list(all_sets.difference(practiced_sets))
        self.context['practiced_sets'] = list(practiced_sets)
        self.context['unpracticed_sets'] = unpracticed_sets
        self.context['username'] = request.user

        return render(request, 'prepare/sets_list.html', self.context)


class Chapters(View):
    context={"chapter_list":True}
    def get(self, request, subject):

        if not check_authentication(request):
            return redirect('index')

        # the student is authencicated
        try:
            subject_obj = Subject.objects.get(slug=subject, exam__name='IOE')
        except Exception:
            return HttpResponse('subject not found')

        # show the chapter lists
        chapters = Chapter.objects.filter(subject=subject_obj)
        if len(chapters)==0:
            raise Http404('sorry but no contents were found')
        self.context['subject'] = subject_obj
        self.context['chapters'] = chapters
        self.context['username'] = request.user
        
        return render(request, 'prepare/sets_list.html', self.context)


class PracticeChapter(View):
    context = {}
    def get(self, request, subject, chapter):

        if not check_authentication(request):
            return redirect('index')

        try:
            subject_obj = Subject.objects.get(slug=subject, exam__name='IOE')
            chapter_obj = Chapter.objects.get(slug=chapter, subject=subject_obj)
        except Exception as e:
            return HttpResponse("<h2>HTTP 404</h2><br/>sorry, the page requested was not found")
        # get all the questions
        send_questions=[]
        questions = Question.objects.filter(chapter=chapter_obj)
        temp_collection = set() # to hold 20 questions
        # send questions in block of 20 each time
        # if less, send all
        if questions.count() < 20:
            send_questions = list(questions)
        else:
            while len(temp_collection) < 20: # keep on adding random question
                temp_collection.add(questions[random.randrange(0,len(temp_collection)-1)])
            send_questions = list(temp_collection)

        self.context['num_questions'] = len(send_questions)
        self.context['questions'] = send_questions
        self.context['time'] = self.context['num_questions'] * 1.5
        self.context['username'] = request.user
        self.context['answers'] = json.dumps([x.correct_answer for x in send_questions])

        return render(request, "prepare/practice.html", self.context)

    def post(self, request, subject, chapter):
        try:
            subject_obj = Subject.objects.get(slug=subject, exam__name='IOE')
            chapter_obj = Chapter.objects.get(slug=chapter, subject=subject_obj)
            total_questions = float(request.POST.get('total_questions',''))
            correct_answers = float(request.POST.get('total_questions',''))

            student = Student.objects.get(user=request.user)

            update_student_profile(student, total_questions, correct_answers)

            return HttpResponse('Your Progress has been saved')
        except Exception as e:
            return HttpResponse(e)
            raise Http404('Request could not be forwarded. May be due to wrong data')


class PracticeSet(View):
    context = {}
    def get(self, request, setid):
        
        if not check_authentication(request):
            return redirect('index')

        try:
            set_obj = QuestionSet.objects.get(id=setid)
        except Exception as e:
            return HttpResponse(e)
            return HttpResponse("<h2>HTTP 404</h2><br/>sorry, the page requested was not found")
        # get all the questions
        questions = Question.objects.filter(question_set=set_obj)
        self.context['num_questions'] = len(questions)
        self.context['questions'] = list(questions)
        self.context['time'] = self.context['num_questions'] * 1.5
        self.context['username'] = request.user
        self.context['url'] = '/prepare/sets/'+str(set_obj.id)+'/'
        # get the question answers
        self.context['answers'] = json.dumps([x.correct_answer for x in questions])
        return render(request, "prepare/practice.html", self.context)

    def post(self, request, setid):
        if not check_authentication(request):
            return redirect('index')
        else:
            try:
                user = request.user
                set_obj = QuestionSet.objects.get(id=setid)
                student = Student.objects.get(user=user)
                total_questions = int(request.POST.get('total_questions', ''))
                correct_answers = int(request.POST.get('correct_answers', ''))

                update_student_profile(student, total_questions, correct_answers)

                return HttpResponse('Your Progress has been saved')

            except Exception:
                return HttpResponse('<h2>HTTP404</h2><br>something wrong. maybe the user was not found. or maybe invalid data received.')


def check_authentication(request):
    return request.user.is_authenticated() and not request.user.is_staff


def update_student_profile(student, total_questions, correct_answers):
    student.questions_done += total_questions
    progress = student.progress
    correct_subs = student.correct_submissions
    correct_subs += correct_answers
    student.correct_submissions = correct_subs
    progress = progress + 5.0**(correct_answers/correct_subs)
    # the increment of progress is decreased with increasing
    #  practices
    student.progress = progress
    student.save()

