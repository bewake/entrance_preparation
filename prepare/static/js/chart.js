//Morris charts snippet - js

$.getScript('http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js',function(){
$.getScript('http://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.0/morris.min.js',function(){

      Morris.Donut({
        element: 'donut-example',
        data: [
         {label: "Physics", value: 50},
         {label: "Mathematics", value: 50},
         {label: "Chemistry", value: 15},
         {label: "Aptitude Test", value: 15},
         {label: "English", value: 15}
        ]
      });
      
      Morris.Bar({
         element: 'bar-example',
         data: [
            {y: 'Physics', a: 15, b:35 ,c:50 },
            {y: 'Mathematics', a: 15,  b: 35 ,c:50},
            {y: 'Chemistry', a: 5,  b: 10 ,c:15},
            {y: 'English', a: 5,  b: 10 ,c: 15},
            {y: 'Aptitude Test', a: 5,  b: 10, c:15},
         ],
         xkey: 'y',
         ykeys: ['a', 'b','c'],
         labels: ['Short Questions', 'Long Questions','Total']
      });
  
});
});
