// variables for time units
var days, hours, minutes, seconds;
 
// get tag element
var countdown = document.getElementById('countdown');

var seconds_left;
var decrement = 1;
var alerted = false;
 
// update the tag with id "countdown" every 1 second
function start_timer(time_in_seconds) {
    seconds_left = time_in_seconds;

    setInterval(function () {
     
        // find the amount of "seconds" between now and target
        //var current_date = new Date().getTime();
        //var seconds_left = (target_date - current_date) / 1000;
     
        // do some time calculations
        days = parseInt(seconds_left / 86400);
        seconds_left = seconds_left % 86400;
         
        hours = parseInt(seconds_left / 3600);
        seconds_left = seconds_left % 3600;
         
        minutes = parseInt(seconds_left / 60);
        seconds = parseInt(seconds_left % 60);
    
        if(seconds_left==0){
            if (alerted==false)
            {
                if (g_submitted==false)
                    window.alert("Your time is finished.");
                alerted = true;
                // calculate number of correct answers
                submit_answers();
            }
            decrement=0;
        }

        seconds_left-=decrement;
         
        // format countdown string + set tag value
        countdown.innerHTML = '<span class="hours">' + hours + ' <b>:</b></span> <span class="minutes">'
        + minutes + ' <b>:</b></span> <span class="seconds">' + seconds + ' </span>';
     
    }, 1000);
}
