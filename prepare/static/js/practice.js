function update_answer(ans_index, value)
{
    if(g_answers[ans_index]==0)
        g_questions_completed+=1;

    update_remaining_questions();

    g_answers[ans_index] = parseInt(value);

    prepare_attempted_details();
}


function update_remaining_questions()
{
    $("#remaining-questions").text("remaining questions: " +
                            String(g_total_questions-g_questions_completed));
}


function prepare_answer_sheet()
{
    var correct = "col-md-2 box box-magic-green col-sm-1";
    var wrong = "col-md-2 box box-magic-red col-sm-1";
    var unattempted = "col-md-2 box box-magic-blue col-sm-1";

    var sheet = document.getElementById("answer-sheet");
    var answer;

    for(var i=0;i<g_total_questions;i++)
    {
        answer = document.createElement("div");

        if(g_answers[i]==0)
        {
            answer.setAttribute("class", unattempted);
        }
        else if (g_answers[i] == g_correct_answers[i])
        {
            answer.setAttribute("class", correct);
        }
        else
        {
            answer.setAttribute("class", wrong);
        }

        answer.setAttribute("id", "answer"+String(i+1));
        answer.innerHTML = String(i+1) + ". "+
                    String.fromCharCode(97 + g_correct_answers[i]-1);
        
        sheet.appendChild(answer);
    }
}


function prepare_attempted_details()
{
    var sheet = document.getElementById("attempt-details");
    var children = sheet.children;

    // first remove any child left
    for(var x=0;x<g_total_questions;x++)
    {
        var elem = document.getElementById("attempted-"+String(x+1))
        if (elem != null)
            elem.remove();
    }

    var answer;
    for(var i=0;i<g_total_questions;i++)
    {
        answer = document.createElement("div");
        if(g_answers[i]==0)
        {
            answer.setAttribute("class", "col-md-2 col-sm-1 box box-magic");
        }
        else
        {
            answer.setAttribute("class", "col-md-2 col-sm-1 box box-magic-blue");
        }
        answer.setAttribute("id", "attempted-"+String(i+1));
        answer.innerHTML = String(i+1) + ". ";
        sheet.appendChild(answer);
    }
}


function submit_answers()
{
    g_submitted = true;
    decrement = 0; // is global
    //seconds_left = 0; // is global
    var elem = document.getElementById('show-answer-sheet');
    elem.setAttribute("href", "#myModal");
    elem.setAttribute("onclick", "");

    prepare_answer_sheet();

    // hide the submit button
    $('#submit-answers').hide();    

    // show the message
    elem = document.getElementById('submit-message');
    elem.style.display="block";

    // hide the remaining questions button
    $('#remaining-questions').hide();

    // post the result to the server
    post_result();
}
