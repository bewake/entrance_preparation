from django.contrib import admin
from prepare.models import *

# Register your models here.

admin.autodiscover()


class QuestionAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'chapter':
            if request.user.is_superuser:
                pass

admin.site.register(Student)
admin.site.register(Question)
admin.site.register(Chapter)
admin.site.register(ExamType)
admin.site.register(DocFile)
admin.site.register(QuestionSet)
admin.site.register(Subject)
