import zipfile
from lxml import etree
import re
import subprocess

def get_word_xml(docx_filename):
    global rels
    zip = zipfile.ZipFile('media/'+docx_filename, 'r')
    zip.extractall('extracted/docx')
    xml_content = zip.read('word/document.xml')
    return xml_content

def get_xml_tree(xml_string):
    return etree.fromstring(xml_string)

def get_rels_xml(docx_filename):
    zip = zipfile.ZipFile('media/'+docx_filename, 'r')
    rels_content = zip.read('word/_rels/document.xml.rels')
    return rels_content


def get_questions_and_options(tree, rels_tree):
    options = ['a', 'b', 'c', 'd']
    optcount = 0
    questions = []
    current_str = ''
    current_tag = 'p'
    count = 0
    answer = False # to check whether text to be put in answers or questions
    answers = ''

    for node in tree.iter(tag=etree.Element):
        if check_element_is(node, 'p'):
            count+=1
            if count==2:
                questions.append(current_str)
                count=0
                current_str=''
        if check_element_is(node, 't'):
            count=0
            current_tag = 't'
            if node.text.strip().lower() == 'answers':
                current_str=''
                answer = True
            if answer:
                answers+=node.text
            else:
                current_str+=(' '+node.text)
        if check_is_image(node, 'blip'):
            path = find_path_for_image(node.attrib.values()[0], rels_tree)
            current_str+='####'+path+'****' # hashes just mean the text is path
        if check_element_is(node, 'ilvl'):
            if node.attrib.values()[0]=='1':# means these are options
                current_str+='  '+options[optcount%4]+'. '
                optcount+=1
    questions.append(current_str)
    questions = [x.strip() for x in questions if x.strip()!='']
    return (questions, answers)


def check_element_is(element, type_char):
    word_schema='http://schemas.openxmlformats.org/wordprocessingml/2006/main'
    return element.tag == '{%s}%s' % (word_schema, type_char)

def check_is_image(element, type_char):
    word_schema = 'http://schemas.openxmlformats.org/drawingml/2006/main'
    return element.tag == '{%s}%s' % (word_schema, type_char)




def parse_question_and_options(string):
    string = string.strip()
    # remove leading number listing
    string = re.sub('[0-9][0-9]*\. ', '', string)
    # first make uniform format (options a. b. c. d. not a) b) c) d) )
    string = re.sub('a\)', 'a. ', string)
    string = re.sub('b\)', 'b. ', string)
    string = re.sub('c\)', 'c. ', string)
    string = re.sub('d\)', 'd. ', string)

    replacement = {'a. ':'&&', 'b. ':'&&', 'c. ':'&&', 'd. ':'&&'}
    replacement = dict((re.escape(k), v) for k, v in replacement.items())

    pattern = re.compile('|'.join(replacement.keys()))
    string = pattern.sub(lambda m: replacement[re.escape(m.group(0))], string)

    return list(map(lambda m: m.strip(), string.split('&&')))

def parse_answers(answers_text):
    answers_text = answers_text.lower()
    answers_text = re.sub('answers', '', answers_text)
    answers_text = answers_text.strip()
    answers_text = re.sub('[0-9][0-9]*\.', '&&', answers_text)
    answers_text = answers_text.split('&&')
    answers = [x.strip() for x in answers_text if x.strip()!='']
    return answers


def find_path_for_image(rId, rels_tree):
    for node in rels_tree.iter():
        if node.attrib.has_key('Id') and node.attrib['Id'] == rId: return node.attrib['Target']


# to separate the textual and image part of the question/option
def get_text_and_image(txtandimage, question, detail):
    # find text between #### and ****
    image_path = find_between('####', '****', txtandimage)

    if image_path=='':# means no image
        separated = [txtandimage.split('####')[0], image_path]
        return separated

    # get the new path using objects id, where image is saved
    filename = 'question'+str(question.id)+detail+'.'+image_path.split('.')[-1]
    image_path = "extracted/docx/word/"+image_path
    new_path = "media/images/"+filename
    subprocess.call(["mv", image_path, new_path])
    separated = [txtandimage.split('####')[0], filename]
    print(separated)
    return separated

def find_between(start, end, txt):
    try:
        a = txt.index(start)+len(start)
        b = txt.index(end)
        return txt[a:b]
    except ValueError:
        return ''
