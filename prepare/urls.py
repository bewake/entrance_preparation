from django.conf.urls import patterns, include, url
from django.contrib import admin
from prepare.views import *
from django.conf import settings
from django.conf.urls.static import static

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'entrance_preparation.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^$', Home.as_view(), name='index'),
    url(r'^admin/', Admin.as_view(), name='admin'),
    url(r'^delete/', DeleteSets.as_view(), name='delete'),
    url(r'^login/',custom_login, name='login'),
    url(r'^practices/', Practices.as_view(), name='practices'),
    url(r'^logout/', custom_logout, name='logout'),
    url(r'^register/', Register.as_view(), name='register'),
    url(r'^verify/', verify_account, name='verify'),
    url(r'^questions/', questions, name='questions'),
    url(r'^upload/', UploadQuestions.as_view(), name='upload'),
    url(r'^sets/$', Sets.as_view(), name='set_list'),
    url(r'^sets/(?P<setid>[0-9]+)/$', PracticeSet.as_view(), name='practice_set'),
    url(r'^(?P<subject>[a-z0-9\-]+)/$', Chapters.as_view(), name='chapters_list'),
    url(r'^(?P<subject>[a-z0-9\-]+)/(?P<chapter>[a-z0-9\-]+)/$',
            PracticeChapter.as_view(), name='practice_chapter'),
)
urlpatterns+= static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns+=patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
                            { 'document_root': settings.MEDIA_ROOT, }),
    )
