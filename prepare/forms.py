from django import forms

class RegistrationForm(forms.Form):
    first_name = forms.CharField(label='First Name', max_length=100,
                widget=forms.TextInput(attrs={'class':
                                            'form-control',
                                            'placeholder':'First Name'}
    ))
    last_name = forms.CharField(label='Last Name', max_length=100,
                widget=forms.TextInput(attrs={'class':
                                            'form-control',
                                            'placeholder':'Last Name'}
    ))
    email = forms.EmailField(label='Email', max_length=100,
                 widget=forms.TextInput(attrs={'class':
                                            'form-control',
                                            'placeholder':'Email'}
    ))
    confirm_email = forms.EmailField(label='Confirm Email', max_length=100,
                 widget=forms.TextInput(attrs={'class':
                                            'form-control',
                                            'placeholder':'Confirm Email'}
    ))
    username = forms.CharField(label='Username', max_length=100,
                 widget=forms.TextInput(attrs={'class':
                                            'form-control',
                                            'placeholder':'Username'}
    ))
    college = forms.CharField(label='College', max_length=100,
                 widget=forms.TextInput(attrs={'class':
                                            'form-control',
                                            'placeholder':'College'}
    ))
    password = forms.CharField(label='Password', 
                 widget=forms.PasswordInput(attrs={'class':
                                            'form-control',
                                            'placeholder':'Password'}
    ))
    confirm_password = forms.CharField(label='Confirm password', 
                 widget=forms.PasswordInput(attrs={'class':
                                            'form-control',
                                            'placeholder':'Confirm Password'}
    ))

    def clean(self):
        pw1 = self.cleaned_data.get('password')
        pw2 = self.cleaned_data.get('confirm_password')
        email1 = self.cleaned_data.get('email')
        email2 = self.cleaned_data.get('confirm_email')
        if not email1:
            raise forms.ValidationError('you must confirm your email')
        if not pw2:
            raise forms.ValidationError('you must confirm your password')
        if pw1 and pw2:
            if pw1!=pw2:
                raise forms.ValidationError('The two password fields did not match')
        if email1 and email2:
            if email1!=email2:
                raise forms.ValidationError('The two email fields did not match')
        return self.cleaned_data


class QuestionUploadForm(forms.Form):
    docfile = forms.FileField(
            label='Select a file',
    )
    subject = forms.CharField(
            label='Subject name(if if this is subject wise question)',
            required = False,
    )
    chapter = forms.CharField(
            label='chapter name(if this is subject wise question)',
            required = False,
    )
    set_name = forms.CharField(
            label='question set name(if this is a question set)',
            required = False,
    )


